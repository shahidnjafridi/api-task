# Bank API task

This app is developed using Symfony 4.1 into Docker containers using docker-compose tool.

# Installation

First, clone this repository:

```bash
$ git clone https://bitbucket.org/shahidnjafridi/api-task.git
```

Then, run:

```bash
$ docker-compose build
$ docker-compose up
```

Run

 ```bash
 tool/setup.sh
 ```
 
It will create database and schema for dev and test environment, and run php composer install and clear cache.


You are done, you can access GET http://api.local/transactions (adding http://api.local in system hosts file pointing to localhost)

#####API Documentation:
You can access http://localhost:8000, it will open swagger ui page with api documentation.

API documentation is made using swagger(https://swagger.io/). There is an api yaml file for swagger (docker/swagger/api.yaml), All endpoints with HTTP methods are well explained with descriptions.

#####Tests
Run tool/test.sh.

From tools/bash-php.sh , you can also run bin/phpunit to run the test.



