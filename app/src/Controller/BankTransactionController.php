<?php

namespace App\Controller;

use App\Service\Bank\Transaction;
use App\Service\Bank\TransactionFinderInterface;
use App\Service\Bank\TransactionManagerInterface;
use App\Service\Bank\TransactionPart\TransactionPartFactory;
use App\Service\TransactionDataValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BankTransactionController
{
    /**
     * @var TransactionFinderInterface
     */
    private $transactionFinder;
    
    /**
     * @var TransactionManagerInterface
     */
    private $transactionManager;
    
    /**
     * @var TransactionPartFactory
     */
    private $transactionPartFactory;
    
    /**
     * @var TransactionDataValidator
     */
    private $transactionDataValidator;
    
    /**
     * @param TransactionFinderInterface  $transactionFinder
     * @param TransactionManagerInterface $transactionManager
     * @param TransactionPartFactory      $transactionPartFactory
     * @param TransactionDataValidator    $transactionDataValidator
     */
    public function __construct(
        TransactionFinderInterface $transactionFinder,
        TransactionManagerInterface $transactionManager,
        TransactionPartFactory $transactionPartFactory,
        TransactionDataValidator $transactionDataValidator
    ) {
        
        $this->transactionFinder = $transactionFinder;
        $this->transactionManager = $transactionManager;
        $this->transactionPartFactory = $transactionPartFactory;
        $this->transactionDataValidator = $transactionDataValidator;
    }
    
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function createAction(Request $request): Response
    {
        $requestData = json_decode($request->getContent(), true);
    
        // All exceptions are handled in ApiExceptionSubscriber to proper response
        if (!is_array($requestData)) {
            throw new \Exception('Malformed data in body contents');
        }
    
        $errors = $this->transactionDataValidator->validate($requestData);
        
        if (count($errors) === 0) {
            $transaction = $this->createTransaction($requestData);
    
            $code   = Response::HTTP_CREATED;
            $result = [
                'code'    => $code,
                'message' => 'New transaction have been created',
                'data'    => $transaction,
            ];
        } else {
            $code   = Response::HTTP_UNPROCESSABLE_ENTITY;
            $result = [
                'error' => [
                    'code'    => $code,
                    'message' => 'The transaction could not be created due to validation errors',
                    'errors'  => $errors,
                ],
            ];
        }
        
        return new JsonResponse($result,  $code);
    }
    
    /**
     * @param int $uuid
     *
     * @return Response
     */
    public function getAction(string $id, Request $request): Response
    {
        $code   = Response::HTTP_OK;
        $result = $this->transactionFinder->findByUuid($id);
        if (!$result) {
            $code   = Response::HTTP_NOT_FOUND;
            $result = [
                'code'    => $code,
                'message' => 'Transaction not found.',
            ];
        }
        
        return  new JsonResponse($result,  $code);
    }
    
    /**
     * @return Response
     */
    public function getAllAction(): Response
    {
        $all = $this->transactionFinder->findAll();
    
        return new JsonResponse($all,  Response::HTTP_OK);
    }
    
    /**
     * @param string $id
     *
     * @return Response
     */
    public function deleteAction(string $id): Response
    {
        $transaction = $this->transactionFinder->findByUuid($id);
        
        if (!$transaction) {
            $code   = Response::HTTP_NOT_FOUND;
            $result = [
                'code'    => $code,
                'message' => 'Transaction not found.',
            ];
        } else {
            $this->transactionManager->delete($transaction);
            $code   = Response::HTTP_OK;
            $result = [
                'code'    => $code,
                'message' => 'Transaction has been deleted',
            ];
        }
    
        return new JsonResponse($result,  $code);
    }
    
    /**
     * @param array $requestData
     *
     * @return Transaction
     */
    private function createTransaction(array $requestData)
    {
        $transaction = new Transaction();
        $transaction->setAmount($requestData['amount']);
        $transaction->setBookingDate(new \DateTime($requestData['booking_date']));
        foreach ($requestData['parts'] as $part) {
            $transactionPart = $this->transactionPartFactory->create($part['reason']);
            $transactionPart->setAmount($part['amount']);
            $transaction->addPartTransaction($transactionPart);
        }
        
        return $this->transactionManager->create($transaction);
    }
}
