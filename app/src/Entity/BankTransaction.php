<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BankTransactionRepository")
 */
class BankTransaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", unique=true, length=36)
     */
    private $uuid;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\Column(type="datetime")
     */
    private $bookingDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BankTransactionPart", mappedBy="bankTransaction", orphanRemoval=true)
     */
    private $bankTransactionParts;

    public function __construct()
    {
        $this->bankTransactionParts = new ArrayCollection();
        $this->uuid = Uuid::uuid4()->toString();
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }
    
    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }
    
    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    
    /**
     * @param float $amount
     *
     * @return BankTransaction
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
    
    /**
     * @return \DateTimeInterface|null
     */
    public function getBookingDate(): ?\DateTimeInterface
    {
        return $this->bookingDate;
    }
    
    /**
     * @param \DateTimeInterface $bookingDate
     *
     * @return BankTransaction
     */
    public function setBookingDate(\DateTimeInterface $bookingDate): self
    {
        $this->bookingDate = $bookingDate;

        return $this;
    }

    /**
     * @return Collection|BankTransactionPart[]
     */
    public function getBankTransactionParts(): Collection
    {
        return $this->bankTransactionParts;
    }
    
    /**
     * @param BankTransactionPart $bankTransactionPart
     *
     * @return BankTransaction
     */
    public function addBankTransactionPart(BankTransactionPart $bankTransactionPart): self
    {
        if (!$this->bankTransactionParts->contains($bankTransactionPart)) {
            $this->bankTransactionParts[] = $bankTransactionPart;
            $bankTransactionPart->setBankTransaction($this);
        }

        return $this;
    }
    
    /**
     * @param BankTransactionPart $bankTransactionPart
     *
     * @return BankTransaction
     */
    public function removeBankTransactionPart(BankTransactionPart $bankTransactionPart): self
    {
        if ($this->bankTransactionParts->contains($bankTransactionPart)) {
            $this->bankTransactionParts->removeElement($bankTransactionPart);
            // set the owning side to null (unless already changed)
            if ($bankTransactionPart->getBankTransaction() === $this) {
                $bankTransactionPart->setBankTransaction(null);
            }
        }

        return $this;
    }
}
