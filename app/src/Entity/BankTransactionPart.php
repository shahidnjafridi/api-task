<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BankTransactionPartRepository")
 */
class BankTransactionPart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankTransaction", inversedBy="bankTransactionParts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bankTransaction;
    
    /**
     * @ORM\Column(type="float")
     */
    private $amount;
    
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('payment_request', 'debtor_payback', 'bank_charge', 'unidentified')")
     */
    private $reason;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return BankTransaction
     */
    public function getBankTransaction(): BankTransaction
    {
        return $this->bankTransaction;
    }
    
    /**
     * @param BankTransaction $bankTransaction
     *
     * @return BankTransactionPart
     */
    public function setBankTransaction(BankTransaction $bankTransaction): self
    {
        $this->bankTransaction = $bankTransaction;
        
        return $this;
    }
    
    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
    
    /**
     * @param float $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }
    
    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }
    
    /**
     * @param string $reason
     */
    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }
}
