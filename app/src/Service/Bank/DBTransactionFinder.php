<?php

namespace App\Service\Bank;

use App\Entity\BankTransaction as DBTransaction;
use App\Repository\BankTransactionRepository;
use App\Service\Bank\TransactionPart\TransactionPartFactory;

class DBTransactionFinder implements TransactionFinderInterface
{
    /**
     * @var BankTransactionRepository
     */
    private $transactionRepository;
    
    /**
     * @var TransactionPartFactory
     */
    private $transactionPartFactory;
    
    /**
     *
     * @param BankTransactionRepository $transactionRepository
     * @param TransactionPartFactory    $transactionPartFactory
     */
    public function __construct(
        BankTransactionRepository $transactionRepository,
        TransactionPartFactory $transactionPartFactory
    ) {
        $this->transactionRepository  = $transactionRepository;
        $this->transactionPartFactory = $transactionPartFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function findByUuid(string $uUid): ?Transaction
    {
        $dbTransaction = $this->transactionRepository->findOneBy(['uuid' => $uUid]);
        
        return $this->prepareTransactionData($dbTransaction);
    }
    
    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        $transactionData = [];
        foreach ($this->transactionRepository->findAll() as $dbTransaction) {
            $transactionData[] = $this->prepareTransactionData($dbTransaction);
        }
        
        return $transactionData;
    }
    
    /**
     * @param \App\Entity\BankTransaction $dbTransaction
     *
     * @return Transaction
     */
    private function prepareTransactionData(DBTransaction $dbTransaction): Transaction
    {
        $transaction = new Transaction();
        $transaction->setId($dbTransaction->getId());
        $transaction->setUuid(new Uuid($dbTransaction->getUuid()));
        $transaction->setAmount($dbTransaction->getAmount());
        $transaction->setBookingDate($dbTransaction->getBookingDate());
        foreach ($dbTransaction->getBankTransactionParts() as $part) {
            $transactionPart = $this->transactionPartFactory->create($part->getReason());
            $transactionPart->setId($part->getId());
            $transactionPart->setAmount($part->getAmount());
            $transactionPart->setBankTransaction($transaction);
            $transaction->addPartTransaction($transactionPart);
        }
        
        return $transaction;
    }
}
