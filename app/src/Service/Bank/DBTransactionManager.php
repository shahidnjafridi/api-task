<?php

namespace App\Service\Bank;

use App\Entity\BankTransaction;
use App\Entity\BankTransactionPart;
use App\Repository\BankTransactionRepository;
use Doctrine\ORM\EntityManagerInterface;

class DBTransactionManager implements TransactionManagerInterface
{
    /**
     * @var BankTransactionRepository
     */
    private $transactionRepository;
    
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    
    /**
     * @param EntityManagerInterface    $entityManager
     * @param BankTransactionRepository $transactionRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        BankTransactionRepository $transactionRepository
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->entityManager         = $entityManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public function create(Transaction $transaction): Transaction
    {
        $DbTransaction = new BankTransaction();
        $DbTransaction->setAmount($transaction->getAmount());
        $DbTransaction->setBookingDate($transaction->getBookingDate());
        
        foreach ($transaction->getPartTransactions() as $transactionPart) {
            $DbTransactionPart = new BankTransactionPart();
            $DbTransactionPart->setAmount($transactionPart->getAmount());
            $DbTransactionPart->setReason($transactionPart->getReason());
            $DbTransactionPart->setBankTransaction($DbTransaction);
            $DbTransaction->addBankTransactionPart($DbTransactionPart);
            $this->entityManager->persist($DbTransactionPart);
        }
        
        
        $this->entityManager->persist($DbTransaction);
        $this->entityManager->flush();
        
        $transaction->setUuid(new Uuid($DbTransaction->getUuid()));
        
        return $transaction;
    }
    
    /**
     * {@inheritdoc}
     */
    public function delete(Transaction $transaction): void
    {
        $transaction = $this->transactionRepository->find($transaction->getId());
        $this->entityManager->remove($transaction);
        $this->entityManager->flush();
    }
}
