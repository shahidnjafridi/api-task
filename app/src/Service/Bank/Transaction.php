<?php

namespace App\Service\Bank;

use App\Service\Bank\TransactionPart\TransactionPartInterface;

class Transaction implements \JsonSerializable
{
    /**
     * @var int
     */
    private $id;
    
    /**
     * @var Uuid
     */
    private $uuid;
    
    /**
     * @var float
     */
    private $amount;
    
    /**
     * @var \DateTime
     */
    private $bookingDate;
    
    /**
     * @var TransactionPartInterface[]
     */
    private $partTransactions;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
    
    /**
     * @return Uuid
     */
    public function getUuid(): Uuid
    {
        return $this->uuid;
    }
    
    /**
     * @param int $uuid
     */
    public function setUuid(Uuid $uuid): void
    {
        $this->uuid = $uuid;
    }
    
    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
    
    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }
    
    /**
     * @return \DateTime
     */
    public function getBookingDate(): \DateTimeInterface
    {
        return $this->bookingDate;
    }
    
    /**
     * @param \DateTime $bookingDate
     */
    public function setBookingDate(\DateTimeInterface $bookingDate): void
    {
        $this->bookingDate = $bookingDate;
    }
    
    /**
     * @return TransactionPartInterface[]
     */
    public function getPartTransactions(): array
    {
        return $this->partTransactions;
    }
    
    /**
     * @param TransactionPartInterface $partTransaction
     */
    public function addPartTransaction(TransactionPartInterface $partTransaction): void
    {
        $this->partTransactions[] = $partTransaction;
    }
    
    /**
     * @param TransactionPartInterface $partTransactions
     */
    public function setPartTransactions(TransactionPartInterface $partTransactions): void
    {
        $this->partTransactions = $partTransactions;
    }
    
    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'amount'       => $this->getAmount(),
            'booking_date' => $this->getBookingDate()->format('Y-m-d h:i:s'),
            'parts'        => $this->getPartTransactions(),
        ];
    }
}
