<?php

namespace App\Service\Bank;

interface TransactionFinderInterface
{
    /**
     * @param string $uUid
     *
     * @return Transaction|null
     */
    public function findByUuid(string $uUid): ?Transaction;
    
    /**
     * @return array|Transaction[]
     */
    public function findAll(): array;
}
