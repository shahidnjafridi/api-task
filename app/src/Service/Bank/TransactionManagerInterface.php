<?php

namespace App\Service\Bank;

interface TransactionManagerInterface
{
    /**
     * @param Transaction $transaction
     *
     * @return Transaction
     */
    public function create(Transaction $transaction): Transaction;
    
    /**
     * @param Transaction $transaction
     *
     * @return mixed
     */
    public function delete(Transaction $transaction): void;
}
