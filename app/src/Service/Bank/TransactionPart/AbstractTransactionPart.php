<?php

namespace App\Service\Bank\TransactionPart;

use App\Service\Bank\Transaction;

abstract class AbstractTransactionPart implements TransactionPartInterface, \JsonSerializable
{
    /**
     * @var int
     */
    private $id;
    
    /**
     * @var Transaction
     */
    private $bankTransaction;
    
    /**
     * @var float
     */
    private $amount;
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
    
    /**
     * @return Transaction
     */
    public function getBankTransaction(): Transaction
    {
        return $this->bankTransaction;
    }
    
    /**
     * @param Transaction $bankTransaction
     */
    public function setBankTransaction(Transaction $bankTransaction): void
    {
        $this->bankTransaction = $bankTransaction;
    }
    
    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
    
    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }
    
    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'amount' => $this->getAmount(),
            'reason' => $this->getReason(),
        ];
    }
}
