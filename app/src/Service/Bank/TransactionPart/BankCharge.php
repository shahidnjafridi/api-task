<?php

namespace App\Service\Bank\TransactionPart;

class BankCharge extends AbstractTransactionPart
{
    /**
     * {@inheritdoc}
     */
    public function getReason(): string
    {
        return TransactionPartReasonConstants::REASON_BANK_CHARGE;
    }
}
