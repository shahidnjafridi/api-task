<?php

namespace App\Service\Bank\TransactionPart;

class DebtorPayback extends AbstractTransactionPart
{
    /**
     * {@inheritdoc}
     */
    public function getReason(): string
    {
        return TransactionPartReasonConstants::REASON_DEBTOR_PAYBACK;
    }
}
