<?php

namespace App\Service\Bank\TransactionPart;

class PaymentRequest extends AbstractTransactionPart
{
    /**
     * {@inheritdoc}
     */
    public function getReason(): string
    {
        return TransactionPartReasonConstants::REASON_PAYMENT_REQUEST;
    }
}
