<?php

namespace App\Service\Bank\TransactionPart;

class TransactionPartFactory
{
    
    /**
     * @param string $reason
     *
     * @return TransactionPartInterface
     * @throws \InvalidArgumentException
     */
    public function create(string $reason): TransactionPartInterface
    {
        switch ($reason) {
            case TransactionPartReasonConstants::REASON_BANK_CHARGE:
                $transactionPart = new BankCharge();
                break;
            case TransactionPartReasonConstants::REASON_DEBTOR_PAYBACK:
                $transactionPart = new DebtorPayback();
                break;
            case TransactionPartReasonConstants::REASON_PAYMENT_REQUEST:
                $transactionPart = new PaymentRequest();
                break;
            case TransactionPartReasonConstants::REASON_UNIDENTIFIED:
                $transactionPart = new Unidentified();
                break;
            default:
                throw new \InvalidArgumentException('No transaction part is registered for '.$reason);
        }
        
        return $transactionPart;
    }
}
