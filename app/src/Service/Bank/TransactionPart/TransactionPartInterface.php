<?php

namespace App\Service\Bank\TransactionPart;

use App\Service\Bank\Transaction;

interface TransactionPartInterface
{
    /**
     * @param int $id
     */
    public function setId(int $id): void;
    
    /**
     * @param Transaction $transaction
     */
    public function setBankTransaction(Transaction $transaction): void;
    
    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void;
    
    /**
     * @return int
     */
    public function getId(): int;
    
    /**
     * @return Transaction
     */
    public function getBankTransaction(): Transaction;
    
    /**
     * @return float
     */
    public function getAmount(): float;
    
    /**
     * @return string
     */
    public function getReason(): string;
}
