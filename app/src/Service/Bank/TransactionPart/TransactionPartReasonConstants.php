<?php

namespace App\Service\Bank\TransactionPart;

interface TransactionPartReasonConstants
{
    const REASON_PAYMENT_REQUEST = 'payment_request';
    const REASON_DEBTOR_PAYBACK  = 'debtor_payback';
    const REASON_BANK_CHARGE     = 'bank_charge';
    const REASON_UNIDENTIFIED    = 'unidentified';
}
