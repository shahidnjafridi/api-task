<?php

namespace App\Service\Bank\TransactionPart;

class Unidentified extends AbstractTransactionPart
{
    /**
     * {@inheritdoc}
     */
    public function getReason(): string
    {
        return TransactionPartReasonConstants::REASON_UNIDENTIFIED;
    }
}
