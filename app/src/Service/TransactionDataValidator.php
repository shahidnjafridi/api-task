<?php

namespace App\Service;

use App\Service\Bank\TransactionPart\TransactionPartReasonConstants;
use App\Service\Bank\TransactionPart\Unidentified;
use App\Validator\CustomCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validation;

class TransactionDataValidator
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function validate(array $data): array
    {
        
        $errors    = [];
        $validator = Validation::createValidator();
        
        $constraintViolations = $validator->validate($data, $this->getConstraints());
        
        foreach ($constraintViolations as $violation) {
            /** @var ConstraintViolationInterface $violation */
            $errors[] = [
                'field'   => $violation->getPropertyPath(),
                'message' => $violation->getMessage(),
            ];
        }
        
        // If validation passed,
        // Check if main transaction amount is equal to sum of all part amount
        if (empty($errors)) {
            $sumPartAmount = array_sum(array_column($data['parts'], 'amount'));
            if (bccomp($data['amount'], $sumPartAmount, 2) !== 0) {
                $errors[] = [
                    'field'   => '[parts]',
                    'message' => 'Sum of parts transaction should be equal to main transaction amount.',
                ];
            }
        }
        
        return $errors;
    }
    
    /**
     * @return Assert\Collection
     */
    private function getConstraints()
    {
        $constraint = new Assert\Collection([
            'amount'       => new Assert\Required([
                new Assert\NotBlank(),
                new Assert\Type('numeric'),
            ]),
            'booking_date' => new Assert\Required([
                new Assert\NotBlank(),
                new Assert\DateTime(),
            ]),
            'parts'        => new Assert\Required([
                new Assert\Count([
                    'min'        => 1,
                    'minMessage' => 'At least one part transaction should be provided',
                ]),
                new Assert\Type('array'),
                new Assert\All(new CustomCollection([
                    'amount' => new Assert\Required([
                        new Assert\NotBlank(),
                        new Assert\Type('numeric'),
                    ]),
                    'reason' => new Assert\Required([
                        new Assert\Choice([
                            TransactionPartReasonConstants::REASON_BANK_CHARGE,
                            TransactionPartReasonConstants::REASON_DEBTOR_PAYBACK,
                            TransactionPartReasonConstants::REASON_PAYMENT_REQUEST,
                            TransactionPartReasonConstants::REASON_UNIDENTIFIED,
                        ]),
                    ]),
                ])),
            ]),
        ]);
        
        return $constraint;
    }
}
