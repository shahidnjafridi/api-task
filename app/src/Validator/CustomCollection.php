<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraints\Collection;

class CustomCollection extends Collection
{
    public $message = 'You must provide an array.';
}