<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\CollectionValidator;

class CustomCollectionValidator extends CollectionValidator
{
    /**
     * @param            $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!\is_array($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
            return;
        }
        parent::validate($value, $constraint);
    }
}
