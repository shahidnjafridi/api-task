<?php

namespace App\Tests\Service\Bank;

use App\Entity\BankTransaction;
use App\Service\Bank\Transaction;
use App\Service\Bank\TransactionFinderInterface;
use App\Service\Bank\TransactionPart\BankCharge;
use App\Service\Bank\TransactionPart\DebtorPayback;
use App\Service\Bank\TransactionPart\PaymentRequest;
use App\Service\Bank\TransactionPart\TransactionPartInterface;
use App\Service\Bank\TransactionPart\Unidentified;
use App\Tests\DataFixtures\TransactionFixtures;
use App\Tests\Traits\TransactionFixturesLoader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class BankTransactionTest extends WebTestCase
{
    use TransactionFixturesLoader;
    
    /**
     * @var EntityManagerInterface
     */
    private $em;
    
    /**
     * @var Client
     */
    private $client = null;
    
    public function setUp()
    {
        $kernel = self::bootKernel();
        
        $this->em     = $kernel->getContainer()->get('doctrine')->getManager();
        $this->client = static::createClient();
        
        $this->loadTransactionFixtures($this->em);
    }
    
    /**
     * Test GET /transactions
     */
    public function testGetAll()
    {
        $expectedResponse = [
            [
                'amount'       => 12.99,
                'booking_date' => '2018-09-01 12:34:12',
                'parts'        => [
                    
                    [
                        'amount' => 2.09,
                        'reason' => 'payment_request',
                    ],
                    
                    [
                        'amount' => 10.9,
                        'reason' => 'debtor_payback',
                    ],
                ],
            ],
            [
                'amount'       => 112.12,
                'booking_date' => '2018-10-01 07:31:15',
                'parts'        => [
                    
                    [
                        'amount' => 24.38,
                        'reason' => 'payment_request',
                    ],
                    
                    [
                        'amount' => 30.5,
                        'reason' => 'debtor_payback',
                    ],
                    
                    [
                        'amount' => 45.12,
                        'reason' => 'unidentified',
                    ],
                    
                    [
                        'amount' => 12.12,
                        'reason' => 'bank_charge',
                    ],
                ],
            ],
        ];
        
        $this->client->request('GET', '/transactions');
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertSame($expectedResponse, json_decode($this->client->getResponse()->getContent(), true));
    }
    
    /**
     * Test GET /transactions/{uuid}
     */
    public function testGetByUuid()
    {
        $expectedResponse = [
            'amount'       => 112.12,
            'booking_date' => '2018-10-01 07:31:15',
            'parts'        => [
                
                [
                    'amount' => 24.38,
                    'reason' => 'payment_request',
                ],
                
                [
                    'amount' => 30.5,
                    'reason' => 'debtor_payback',
                ],
                
                [
                    'amount' => 45.12,
                    'reason' => 'unidentified',
                ],
                
                [
                    'amount' => 12.12,
                    'reason' => 'bank_charge',
                ],
            ],
        ];
        
        $this->client->request('GET', '/transactions/1fd8a3cc-0e05-49c5-ad70-a3b242f749d0');
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertSame($expectedResponse, json_decode($this->client->getResponse()->getContent(), true));
    }
    
    /**
     * Test POST /transactions
     */
    public function testCreateSuccess()
    {
        $input = [
            'amount'       => 36.50,
            'booking_date' => '2018-05-01 12:01:27',
            'parts'        => [
                
                [
                    'amount' => 24.38,
                    'reason' => 'payment_request',
                ],
                [
                    'amount' => 12.12,
                    'reason' => 'bank_charge',
                ],
            ],
        ];
        
        $expectedResult = [
            'code'    => Response::HTTP_CREATED,
            'message' => 'New transaction have been created',
            'data'    => [
                'amount'       => 36.5,
                'booking_date' => '2018-05-01 12:01:27',
                'parts'        => [
                    
                    [
                        'amount' => 24.38,
                        'reason' => 'payment_request',
                    ],
                    
                    [
                        'amount' => 12.12,
                        'reason' => 'bank_charge',
                    ],
                ],
            ],
        ];
        
        $this->client->request('POST', '/transactions', [], [], [], json_encode($input));
        $this->assertSame(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());
        $this->assertSame($expectedResult, json_decode($this->client->getResponse()->getContent(), true));
    }
    
    /**
     * Test POST /transactions
     */
    public function testCreateValidationFailed()
    {
        $input = [
            'booking_date' => '2018-05-01 12:01:27',
            'parts'        => [
                [
                    'reason' => 'payment_request',
                ],
            ],
        ];
        
        $expectedResult = [
            'error' => [
                'code'    => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => 'The transaction could not be created due to validation errors',
                'errors'  => [
                    [
                        'field'   => '[amount]',
                        'message' => 'This field is missing.',
                    ],
                    [
                        'field'   => '[parts][0][amount]',
                        'message' => 'This field is missing.',
                    ]
                ]
            ]
        ];
        
        $this->client->request('POST', '/transactions', [], [], [], json_encode($input));
        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $this->client->getResponse()->getStatusCode());
        $this->assertSame($expectedResult, json_decode($this->client->getResponse()->getContent(), true));
    }
    
    /**
     * Test DELETE /transactions
     */
    public function testDelete()
    {
        $expectedResult = [
            'code'    => Response::HTTP_OK,
            'message' => 'Transaction has been deleted',
        
        ];
        
        $this->client->request('DELETE', '/transactions/1fd8a3cc-0e05-49c5-ad70-a3b242f749d0');
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertSame($expectedResult, json_decode($this->client->getResponse()->getContent(), true));
    }
}
