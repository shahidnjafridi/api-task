<?php

namespace App\Tests\DataFixtures;

use App\Entity\BankTransaction;
use App\Entity\BankTransactionPart;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\Entity;

class TransactionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $transactionTestData = json_decode(file_get_contents(__DIR__.'/Resources/transactions.json'), true);
        
        foreach ($transactionTestData as $data) {
            $bankTransaction = new BankTransaction();
            $bankTransaction->setUuid($data['uuid']);
            $bankTransaction->setAmount($data['amount']);
            $bankTransaction->setBookingDate(new \DateTime($data['booking_date']));
            foreach ($data['parts'] as $part) {
                $bankTransactionPart = new BankTransactionPart();
                $bankTransactionPart->setAmount($part['amount']);
                $bankTransactionPart->setReason($part['reason']);
                $bankTransactionPart->setBankTransaction($bankTransaction);
                $manager->persist($bankTransactionPart);
                $bankTransaction->addBankTransactionPart($bankTransactionPart);
            }
            
            $manager->persist($bankTransaction);
        }
        $manager->flush();
    }
}
