<?php

namespace App\Tests\Service\Bank;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\BankTransaction;
use App\Service\Bank\Transaction;
use App\Service\Bank\TransactionFinderInterface;
use App\Service\Bank\TransactionPart\BankCharge;
use App\Service\Bank\TransactionPart\DebtorPayback;
use App\Service\Bank\TransactionPart\PaymentRequest;
use App\Service\Bank\TransactionPart\TransactionPartInterface;
use App\Service\Bank\TransactionPart\TransactionPartReasonConstants;
use App\Service\Bank\TransactionPart\Unidentified;
use App\Service\Bank\Uuid;
use App\Tests\Traits\TransactionFixturesLoader;

class DBTransactionFinderTest extends KernelTestCase
{
    use TransactionFixturesLoader;
    
    /**
     * @var TransactionFinderInterface
     */
    private $transactionFinder;
    
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var
     */
    private $expectedData;
    
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $kernel = self::bootKernel();
        
        $this->em = $kernel->getContainer()->get('doctrine')->getManager();
        
        $this->transactionFinder = $kernel->getContainer()->get('app.db_transaction_finder');
        
        $this->loadTransactionFixtures($this->em);
        
        $this->expectedData = $this->buildExpectation();
        
        parent::setUp();
    }
    
    /**
     * Test case for findAll()
     */
    public function testFindAll()
    {
        $transactions = $this->transactionFinder->findAll();
        
        $this->assertEquals(array_values($this->expectedData), $transactions);
    }
    
    /**
     * Test case for FindByUuid()
     */
    public function testFindByUuid()
    {
        $transactions = $this->transactionFinder->findByUuid('674dbe03-26d6-4f92-b245-cafa48725eb7');
        
        $this->assertEquals($this->expectedData['674dbe03-26d6-4f92-b245-cafa48725eb7'], $transactions);
    }
    
    /**
     * @return array
     */
    private function buildExpectation()
    {
        $transactions = [];
        $repo         = $this->em->getRepository(BankTransaction::class);
        foreach ($repo->findAll() as $dbTransaction) {
            $transactions[$dbTransaction->getUuid()] = $this->buildTransactionFromDbTransactionEntity($dbTransaction);
        }
        
        return $transactions;
    }
    
    /**
     * @param BankTransaction $dbTransaction
     *
     * @return Transaction
     */
    private function buildTransactionFromDbTransactionEntity(BankTransaction $dbTransaction)
    {
        $transaction = $this->createTransaction($dbTransaction->getId(), new Uuid($dbTransaction->getUuid()),
            $dbTransaction->getBookingDate(), $dbTransaction->getAmount());
        
        foreach ($dbTransaction->getBankTransactionParts() as $dbPartTransaction) {
            $part = $this->createPartTransaction($dbPartTransaction->getReason(), $dbPartTransaction->getId(),
                $dbPartTransaction->getAmount(), $transaction);
            $transaction->addPartTransaction($part);
        }
        
        return $transaction;
        
    }
    
    /**
     * @param string      $reason
     * @param int         $id
     * @param float       $amount
     * @param Transaction $transaction
     *
     * @return TransactionPartInterface
     */
    private function createPartTransaction(string $reason, int $id, float $amount, Transaction $transaction)
    {
        $parts = [
            TransactionPartReasonConstants::REASON_BANK_CHARGE     => new BankCharge(),
            TransactionPartReasonConstants::REASON_DEBTOR_PAYBACK  => new DebtorPayback(),
            TransactionPartReasonConstants::REASON_PAYMENT_REQUEST => new PaymentRequest(),
            TransactionPartReasonConstants::REASON_UNIDENTIFIED    => new Unidentified(),
        ];
        
        $part = $parts[$reason];
        $part->setId($id);
        $part->setAmount($amount);
        $part->setBankTransaction($transaction);
        
        return $part;
    }
    
    /**
     * @param int                $id
     * @param Uuid               $uuid
     * @param \DateTimeInterface $bookingDate
     * @param float              $amount
     *
     * @return Transaction
     */
    private function createTransaction(int $id, Uuid $uuid, \DateTimeInterface $bookingDate, float $amount)
    {
        $transaction = new Transaction();
        $transaction->setId($id);
        $transaction->setUuid($uuid);
        $transaction->setBookingDate($bookingDate);
        $transaction->setAmount($amount);
        
        return $transaction;
    }
    
}
