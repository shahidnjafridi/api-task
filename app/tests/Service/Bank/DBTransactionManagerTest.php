<?php

namespace App\Tests\Service\Bank;

use App\Entity\BankTransaction;
use App\Entity\BankTransactionPart;
use App\Repository\BankTransactionRepository;
use App\Service\Bank\DBTransactionManager;
use App\Service\Bank\Transaction;
use App\Service\Bank\TransactionPart\TransactionPartInterface;
use App\Service\Bank\Uuid;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DBTransactionManagerTest extends TestCase
{
    /**
     * @var MockObject|BankTransactionRepository
     */
    private $transactionRepository;
    
    /**
     * @var MockObject|EntityManagerInterface
     */
    private $entityManager;
    
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->transactionRepository = $this->createMock(BankTransactionRepository::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
    }
    
    public function testAdd()
    {
        $transactionAmount = 12.44;
        $transactionBookingDate = new \DateTime('2018-01-01 12:07:00');
        $transactionPartAmount = 12.44;
        $transactionReason = 'some_reason';
        $uuid = 'abcd-defg';

        // Input Data
        $transactionPart = $this->createMock(TransactionPartInterface::class);
        $transactionPart->method('getAmount')->willReturn($transactionPartAmount);
        $transactionPart->method('getReason')->willReturn($transactionReason);
        $transaction = $this->createMock(Transaction::class);
        $transaction->method('getBookingDate')->willReturn($transactionBookingDate);
        $transaction->method('getAmount')->willReturn($transactionAmount);
        $transaction->method('getPartTransactions')->willReturn([$transactionPart]);
        $transaction->method('setUuid')->with(new Uuid($uuid));
    
    
        // Expected DB entity to be passed with persist
        $expectedDBTransaction = new BankTransaction();
        $expectedDBTransaction->setUuid($uuid);
        $expectedDBTransaction->setAmount($transactionAmount);
        $expectedDBTransaction->setBookingDate($transactionBookingDate);
        $expectedDBTransactionPart = new BankTransactionPart();
        $expectedDBTransactionPart->setAmount($transactionPartAmount);
        $expectedDBTransactionPart->setBankTransaction($expectedDBTransaction);
        $expectedDBTransactionPart->setReason($transactionReason);
        $expectedDBTransaction->addBankTransactionPart($expectedDBTransactionPart);
        
        $this->entityManager
            ->expects($this->at(0))
            ->method('persist')
            ->with($this->isInstanceOf(BankTransactionPart::class));
    
        $this->entityManager
            ->expects($this->at(1))
            ->method('persist')
            ->with($this->isInstanceOf(BankTransaction::class))
            ->willReturnCallback(function (BankTransaction $bankTransaction) use ($uuid, $expectedDBTransaction) {
                $bankTransaction->setUuid($uuid);
                $this->assertEquals($expectedDBTransaction, $bankTransaction);
            });
        
        $this->entityManager->method('flush');
        
        $dbTransactionManager = new DBTransactionManager($this->entityManager, $this->transactionRepository);
        $this->assertEquals($transaction, $dbTransactionManager->create($transaction));
    }
    
    /**
     * @doesNotPerformAssertions
     */
    public function testDelete()
    {
        $transaction = $this->createMock(Transaction::class);
        $transaction->method('getId')->with(1);

        $dbTransaction = $this->createMock(BankTransaction::class);
        
        // Make sure db entity has all properties set as expected
        $this->entityManager->method('find')->with(1)->willReturn($dbTransaction);
        $this->entityManager->method('remove')->with($dbTransaction);
        $this->entityManager->method('flush');
    }
}
