<?php

namespace App\Tests\Service\Bank;

use App\Service\Bank\TransactionPart\TransactionPartReasonConstants;
use App\Service\TransactionDataValidator;
use PHPUnit\Framework\TestCase;

class TransactionDataValidatorTest extends TestCase
{
    /**
     * @dataProvider provideTestData
     *
     * @param array $data
     * @param array $expectedErrors
     */
    public function testValidate(array $data, array $expectedErrors)
    {
        $validator = new TransactionDataValidator();
        $this->assertEquals($expectedErrors, $validator->validate($data));
    }
    
    /**
     * @return array
     */
    public function provideTestData()
    {
        return [
            'Test case with missing part transaction key'                                        => [
                [
                    'amount'       => 12,
                    'booking_date' => '2018-09-10 14:12:00',
                ],
                [
                    [
                        'field'   => '[parts]',
                        'message' => 'This field is missing.',
                    ],
                ],
            ],
            'Test case with empty part transaction'                                              => [
                [
                    'amount'       => 11,
                    'booking_date' => '2018-09-10 14:12:00',
                    'parts'        => [],
                ],
                [
                    [
                        'field'   => '[parts]',
                        'message' => 'At least one part transaction should be provided',
                    ],
                ],
            ],
            'Test case with provding non-array values in part transaction'                       => [
                [
                    'amount'       => 12,
                    'booking_date' => '2018-09-10 14:12:00',
                    'parts'        => ['amount' => 12.09],
                ],
                [
                    [
                        'field'   => '[parts][amount]',
                        'message' => 'You must provide an array.',
                    ],
                
                ],
            ],
            'Test case with incorrect transaction amount and missing reason in part transaction' => [
                [
                    'amount'       => 'abc',
                    'booking_date' => '2018-09-10 14:12:00',
                    'parts'        => [['amount' => 12.09]],
                ],
                [
                    [
                        'field'   => '[amount]',
                        'message' => 'This value should be of type numeric.',
                    ],
                    [
                        'field'   => '[parts][0][reason]',
                        'message' => 'This field is missing.',
                    ],
                
                
                ],
            ],
            'Test case with missing booking_date and wrong reason in part transaction'           => [
                [
                    'amount' => 12,
                    'parts'  => [['amount' => 12.09, 'reason' => 'abcde']],
                ],
                [
                    [
                        'field'   => '[booking_date]',
                        'message' => 'This field is missing.',
                    ],
                    
                    [
                        'field'   => '[parts][0][reason]',
                        'message' => 'The value you selected is not a valid choice.',
                    ],
                
                ],
            ],
            'Test case with unknown field and wrong booking date format'                         => [
                [
                    'unknown_field' => 'abc',
                    'amount'        => 12,
                    'booking_date'  => '2018-09',
                    'parts'         => [['amount' => 12.09, 'reason' => 'bank_charge']],
                ],
                [
                    [
                        'field'   => '[booking_date]',
                        'message' => 'This value is not a valid datetime.',
                    ],
                    [
                        'field'   => '[unknown_field]',
                        'message' => 'This field was not expected.',
                    ],
                ],
            ],
            
            'Test case with when transaction amount is not equal to sum of part transactions' => [
                [
                    'amount'       => 16.20,
                    'booking_date' => '2018-09-10 14:12:00',
                    'parts'        => [
                        ['amount' => 12.09, 'reason' => TransactionPartReasonConstants::REASON_PAYMENT_REQUEST],
                        ['amount' => 10.91, 'reason' => TransactionPartReasonConstants::REASON_BANK_CHARGE],
                    ],
                ],
                [
                    [
                        'field'   => '[parts]',
                        'message' => 'Sum of parts transaction should be equal to main transaction amount.',
                    ],
                ],
            ],
            'Test case with correct data'                                                     => [
                [
                    'amount'       => 13.00,
                    'booking_date' => '2018-09-10 14:12:00',
                    'parts'        => [
                        ['amount' => 12.09, 'reason' => TransactionPartReasonConstants::REASON_PAYMENT_REQUEST],
                        ['amount' => 0.91, 'reason' => TransactionPartReasonConstants::REASON_BANK_CHARGE],
                    ],
                ],
                [// No errors in case of correct data
                ],
            ],
        ];
    }
}
