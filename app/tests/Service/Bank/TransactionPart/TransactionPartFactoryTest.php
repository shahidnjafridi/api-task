<?php

namespace App\Tests\Service\Bank\TransactionPart;

use App\Service\Bank\TransactionPart\BankCharge;
use App\Service\Bank\TransactionPart\DebtorPayback;
use App\Service\Bank\TransactionPart\PaymentRequest;
use App\Service\Bank\TransactionPart\TransactionPartFactory;
use App\Service\Bank\TransactionPart\Unidentified;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TransactionPartFactoryTest extends KernelTestCase
{
    /**
     * @var TransactionPartFactory
     */
    private $transactionPartFactory;
    
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $this->transactionPartFactory = self::$container->get('app.transaction_part_factory');
    }
    
    /**
     * @dataProvider provideTestData
     *
     * @param string $reason
     * @param string $expectedClassInstance
     */
    public function testCreate(string $reason, string $expectedClassInstance)
    {
        $transactionPart = $this->transactionPartFactory->create($reason);
        
        $this->assertInstanceOf($expectedClassInstance, $transactionPart);
    }
    
    /**
     * @return array
     */
    public function provideTestData()
    {
        return [
            'Test case for bank charge'     => [
                'bank_charge',
                BankCharge::class,
            ],
            'Test case for debtor payback'  => [
                'debtor_payback',
                DebtorPayback::class,
            ],
            'Test case for payment request' => [
                'payment_request',
                PaymentRequest::class,
            ],
            'Test case for unidentified'    => [
                'unidentified',
                Unidentified::class,
            ],
        ];
    }
}
