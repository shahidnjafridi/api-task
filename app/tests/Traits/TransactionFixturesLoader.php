<?php

namespace App\Tests\Traits;

use App\Tests\DataFixtures\TransactionFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;

trait TransactionFixturesLoader
{
    public function loadTransactionFixtures(EntityManagerInterface $em)
    {
        $loader = new Loader();
        $loader->addFixture(new TransactionFixtures());
    
        $purger   = new ORMPurger($em);
        $executor = new ORMExecutor($em, $purger);
        $executor->execute($loader->getFixtures());
    }
}
