#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR}/../.env"
cd "${DIR}/.."

CMD=${1:-bash}

docker-compose exec php ${CMD} ${@:2}