#!/bin/bash -x

if [[ -n $1 ]] && [[ "$1" -eq "full" ]]; then
    echo "Removing cache folders..."
    docker-compose exec php rm -r var/cache/
fi

echo "Clearing cache..."
docker-compose exec php bin/console cache:clear