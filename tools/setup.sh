#!/bin/bash -ex

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR}/../.env"
cd "${DIR}/.."

echo "Composer install"
docker-compose exec php composer install


docker-compose exec -T mysql mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS \`bank\` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
docker-compose exec -T mysql mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS \`test_bank\` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"

echo "Migrations ..."
docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction

echo "Clearing cache..."
docker-compose exec php bin/console cache:clear
docker-compose exec php bin/console cache:clear --env=test

echo "Updating test databases"
docker-compose exec php bin/console doctrine:schema:update --force -e test